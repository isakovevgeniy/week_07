package ru.edu;

import ru.edu.model.Symbol;

import java.util.Map;

/**
 * Сервис для получения котировок, должен быть thread-safe.
 */
public interface SymbolPriceService {

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш,
     * с помощью которого данные будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName symbol
     * @return price
     */
    Symbol getPrice(String symbolName);

    /**
     * получение данных по всем тикерам.
     *
     * @return map
     */
    Map<String, Symbol> getSymbols();
}
