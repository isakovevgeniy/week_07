package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class CachedSymbolPriceService {
    /**
     * delay.
     */
    private static final Long DELAY_IN_SECONDS = 5L;
    /**
     * delegate.
     */
    private final SymbolPriceService delegate;
    /**
     * cache.
     */
    private final Map<String, Symbol> cache = new HashMap<>();

    /**
     * constructor.
     * @param pDelegate
     */
    public CachedSymbolPriceService(final SymbolPriceService pDelegate) {
        delegate = pDelegate;
    }

    /**
     * logger.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(CachedSymbolPriceService.class);
    /**
     * url.
     */
    private static final String RESOURCE_URL =
            "https://api.binance.com/api/v3/ticker/price";
    /**
     * restTemplate.
     */
    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные
     * будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName symbol
     * @return Symbol
     */
    public Symbol getPrice(final String symbolName) {
        Instant timeToUpdateCache = Instant.ofEpochMilli(0);
        if (cache.get(symbolName) != null) {
            timeToUpdateCache = cache.get(symbolName)
                    .getTimeStamp()
                    .plusSeconds(DELAY_IN_SECONDS);
        }
        if (!cache.containsKey(symbolName) || Instant.now()
                .isAfter(timeToUpdateCache)) {
            cache.put(symbolName, delegate.getPrice(symbolName));
            LOGGER.debug("It's time to update cache");
        } else {
            LOGGER.debug("read data from cache");
        }
        return cache.get(symbolName);
    }
}

