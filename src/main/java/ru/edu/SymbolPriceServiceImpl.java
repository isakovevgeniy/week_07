package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SymbolPriceServiceImpl implements SymbolPriceService {
    /**
     * logger.
     */
    private static final Logger LOGGER = LoggerFactory
            .getLogger(SymbolPriceServiceImpl.class);
    /**
     * url.
     */
    private static final String RESOURCE_URL =
            "https://api.binance.com/api/v3/ticker/price";
    /**
     * restTemplate.
     */
    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные
     * будут обновляться не чаще,
     * чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        String paramName = "?symbol=";
        ResponseEntity<SymbolImpl> response = restTemplate
                .getForEntity(RESOURCE_URL
                        + paramName
                        + symbolName, SymbolImpl.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            LOGGER.error("Не получили ответ. Код ответа {}",
                    response.getStatusCode());
            return null;
        }
        SymbolImpl symbol = response.getBody();
        LOGGER.debug("Получили ответ для {}: {}", symbolName, symbol);
        return symbol;
    }

    /**
     * map.
     * @return map
     */
    public Map<String, Symbol> getSymbols() {
        Map<String, Symbol> symbolMap = new HashMap<>();

        RequestEntity<Void> requestEntity = RequestEntity.get(RESOURCE_URL)
                .header("X-Forwarded-For", "127.0.0.1")
                .header("Cookie", "name=val")
                .accept(MediaType.APPLICATION_JSON)
                .build();

        ResponseEntity<SymbolImpl[]> response = restTemplate.
                exchange(requestEntity, SymbolImpl[].class);
        if (response.getStatusCode() != HttpStatus.OK) {
            LOGGER.error("Не получили ответ. Код ответа {}",
                    response.getStatusCode());
            return null;
        }
        Arrays.stream(response.getBody())
                .forEach(e -> symbolMap.put(e.getSymbol(), e));
        LOGGER.debug("Получили ответ: {}", symbolMap);
        return symbolMap;
    }
}

