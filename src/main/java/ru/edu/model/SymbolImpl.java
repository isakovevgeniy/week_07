package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {
    /**
     * ticker.
     */
    private String symbol;
    /**
     * price.
     */
    private BigDecimal price;
    /**
     * updateTime.
     */
    private final Instant timeStamp = Instant.now();

    /**
     * default.
     */
    public SymbolImpl() {

    }

    /**
     * for test.
     * @param pSymbol - symbol
     * @param pPrice -price
     */
    public SymbolImpl(final String pSymbol, final BigDecimal pPrice) {
        symbol = pSymbol;
        price = pPrice;
    }

    /**
     * getter.
     * @return symbol
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * getter.
     * @return price
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Время получения данных.
     *
     * @return время
     */
    @Override
    public Instant getTimeStamp() {
        return timeStamp;
    }

    /**
     * toString.
     * @return строка
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                + "symbol='" + symbol + '\''
                + ", price=" + price
                + ", timeStamp=" + timeStamp
                + '}';
    }
}

