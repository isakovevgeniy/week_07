package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {
    /**
     * getter.
     * @return symbol
     */
    String getSymbol();

    /**
     * getter.
     * @return price
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return time
     */
    Instant getTimeStamp();
}
