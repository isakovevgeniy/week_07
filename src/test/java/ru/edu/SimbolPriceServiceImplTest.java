package ru.edu;

import junit.framework.TestCase;
import org.junit.Test;
import ru.edu.model.Symbol;

import java.util.Map;

public class SimbolPriceServiceImplTest extends TestCase {

    @Test
    public void testGetPrice() {
        SymbolPriceService priceService = new SymbolPriceServiceImpl();
        Symbol symbol = priceService.getPrice("BTCUSDT");
        assertNotNull(symbol);
    }
    @Test
    public void testSymbols() {
        SymbolPriceService priceService = new SymbolPriceServiceImpl();
        Map<String,Symbol> symbolMap = priceService.getSymbols();
        assertNotNull(symbolMap);
    }
}