package ru.edu;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import ru.edu.model.Symbol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CachedSymbolPriceServiceImplTest {
    public static final String SYMBOL_NAME = "BTCUSDT";

    SymbolPriceService symbolPriceService = new SymbolPriceServiceImpl();
    CachedSymbolPriceService cachedSymbolPriceService = new CachedSymbolPriceService(symbolPriceService);

    SymbolPriceService symbolPriceServiceSpy = Mockito.spy(symbolPriceService);
    CachedSymbolPriceService cachedSpy = Mockito.spy(cachedSymbolPriceService);


    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getPrice() throws InterruptedException {
        for (int i = 0; i < 6; i++) {
            cachedSpy.getPrice(SYMBOL_NAME);
            Thread.sleep(1000);
        }
        Mockito.verify(cachedSpy, Mockito.times(6)).getPrice(Mockito.any());
//        Mockito.verify(symbolPriceServiceSpy, Mockito.times(2)).getPrice(Mockito.any()); // there were zero interactions with this mock.
    }

    @Test
    public void getPrice_2() throws InterruptedException {

        Symbol symbol = cachedSymbolPriceService.getPrice(SYMBOL_NAME);
        Symbol symbolAgain = cachedSymbolPriceService.getPrice(SYMBOL_NAME);
        System.out.println(symbol.getTimeStamp());
        System.out.println(symbolAgain.getTimeStamp());
        assertEquals(symbol.getTimeStamp(), symbolAgain.getTimeStamp());

        System.out.println("Ждем-с");
        Thread.sleep(6_000L);

        Symbol symbolNew = cachedSymbolPriceService.getPrice(SYMBOL_NAME);
        System.out.println(symbolNew.getTimeStamp());
        assertTrue(symbol.getTimeStamp().isBefore(symbolNew.getTimeStamp()));
    }


}