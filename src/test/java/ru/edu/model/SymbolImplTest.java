package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class SymbolImplTest {
    Symbol symbol = new SymbolImpl("A", BigDecimal.valueOf(9.01));


    @Test
    public void getSymbol() {
        assertEquals("A", symbol.getSymbol());
    }

    @Test
    public void getPrice() {
        assertEquals(BigDecimal.valueOf(9.01), symbol.getPrice());
    }

    @Test
    public void getTimeStamp() {
        assertNotNull(String.valueOf(symbol.getTimeStamp()), "alarma el ritmo fatal"); // непонятно зачем потребовалось оборачивать в стринг. но по-другому не бралось
    }

    @Test
    public void testToString() {
        assertTrue(symbol.toString().matches("SymbolImpl\\{symbol='A', price=9.01, timeStamp=.*\\}"));
    }
}